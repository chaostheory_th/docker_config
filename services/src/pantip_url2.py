# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup
import time
from datetime import datetime
from date import date_thai_convert
import main_config
from pymongo import MongoClient
from queue import Queue
from threading import Thread

client = MongoClient(main_config.MONGODB_CONFIG['host'],
                     username=main_config.MONGODB_CONFIG['username'],
                     password=main_config.MONGODB_CONFIG['password']
                     )

mydb = client['pantip_190710']
mycol = mydb['pantip_content_new']
start_date = datetime.strptime('20/8/2019 00:00:00', '%d/%m/%Y %H:%M:%S')
end_date = datetime.strptime('15/8/2019 23:59:59', '%d/%m/%Y %H:%M:%S')


def pantip_scrape_urls(url, category, start_date, end_date):

    content_urls = []
    browser = webdriver.Remote(
        desired_capabilities=webdriver.DesiredCapabilities.FIREFOX,
        command_executor=main_config.SELENIUM_CONFIG['HOST'])
    browser.get(url)
    print(url)
    try:
        # browser = webdriver.Firefox(executable_path='/home/songsit/PycharmProjects/pantip/geckodriver')
        SCROLL_PAUSE_TIME = 1
        browser.execute_script("return document.body.scrollHeight")

        i = 1
        load_page = True
        while load_page:
            html = browser.page_source
            html_soup = BeautifulSoup(html, 'html5lib')
            remove_ad = html_soup.findAll('li', class_='pt-list-item ad-slot')
            [y.decompose() for y in remove_ad]
            posts = html_soup.find('div', id='pt-topic-left')
            content = posts.findAll('li', class_='pt-list-item')
            for x in content:
                p = x.find('div', class_='pt-list-item__info')
                time_format = date_thai_convert(p.find('span').text)
                if time_format < end_date:
                # if time_format <= end_date and time_format > start_date:
                    load_page = False

            # Scroll down to bottom
            browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")

            # Wait to load page
            time.sleep(SCROLL_PAUSE_TIME)

            # Calculate new scroll height and compare with last scroll height
            browser.execute_script("return document.body.scrollHeight")
            print('page ', i)
            i += 1

        html = browser.page_source
        html_soup = BeautifulSoup(html, 'html5lib')
        posts = html_soup.find('div', id='pt-topic-left')
        content = posts.findAll('li', class_='pt-list-item')
        for x in content:
            p = x.find('a', href=True)
            data = {
                'domain_name': url,
                'datasource_url': p['href'],
                'category': category,
                'status': 0
            }
            content_urls.append(data)

        browser.delete_all_cookies()
        browser.close()
    except (TimeoutException, Exception) as e:
        browser.delete_all_cookies()
        browser.close()
        print(e)

    return content_urls


def crawl(url, cat, start_date, end_date):
    try:
        ans = pantip_scrape_urls(url, cat, start_date, end_date)
        return ans
    except Exception as e:
        print(e)
        pass


def worker():
    while True:

        item = q.get()
        crawl_data = crawl(item[0], item[1], item[2], item[3])
        if crawl_data:
            import_data.extend(crawl_data)
        q.task_done()


q = Queue()

# set thread
for i in range(80):
    t = Thread(target=worker)
    t.daemon = True
    t.start()


def chunker(seq, size):
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))


file = open("pantip_room.txt", "r")
count_time = 0
ss = datetime.now()
for group in chunker(list(file), 4):
    import_data = []
    for list_data in group:
        q.put([list_data.split('|')[0], list_data.split('|')[1], start_date, end_date])

    q.join()

    if import_data:
        mycol.insert_many(import_data)
        print('import to db: ', len(import_data))
        print(datetime.now() - ss)