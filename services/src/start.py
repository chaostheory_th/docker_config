import sys
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Index Page'

@app.route('/hello')
def hello():
    return 'Hello, World'

@app.route('/home')
def home():
    return 'Hello, home'

@app.route('/version')
def version():
    return 'Python Version: ' + str(sys.version)

app.run(debug=True,host='0.0.0.0')
