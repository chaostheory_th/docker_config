DATABASE_CONFIG_URLS = {
    'mysql': {
        'driver': 'mysql',
        'host': '192.168.1.21',
        'database': 'ohm',
        'user': 'root',
        'password': 'theory@2018#',
    }
}


DATABASE_CONFIG_INSERT = {
    'mysql': {
        'driver': 'mysql',
        'host': '192.168.1.21',
        'database': 'test_data',
        'user': 'root',
        'password': 'theory@2018#',
    }
}


DATABASE_CONFIG = {
    'mysql': {
        'driver': 'mysql',
        'host': 'mariadb',
        'database': 'new_data',
        'user': 'root',
        'password': 'theory@2018#',
        'charset': 'UTF8'

    }
}

SELENIUM_CONFIG = {
    'HOST': 'http://selenium-hub-v2:4444/wd/hub'
}

MONGODB_CONFIG = {
    'host': 'mongo',
    'username': 'root',
    'password': 'theory@2018#'
}

TEST = {
    'honda': {
        'key_list': ['honda', 'Honda', 'HONDA', 'ฮอนด้า', 'ฮอนดา'],
        'collection_name': 'dbhonda_190101190701',
        'main_key': 'honda'
    },
    'toyota': {
        'key_list': ['toyota', 'Toyota', 'TOYOTA', 'โตโยต้า', 'โตโยตา'],
        'collection_name': 'dbtoyota_190101190701',
        'main_key': 'toyota'
    }
}